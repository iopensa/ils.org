# Credits
# https://gitlab.com/ItalianLinuxSociety/nikola-docker

# Nikola requirements
nikola==8.2.4

# Dependencies required by Nikola
beautifulsoup4==4.9.3
bs4==0.0.1
certifi==2021.5.30
chardet==4.0.0
idna==3.2
requests==2.26.0
urllib3==1.26.7
pyyaml==6.0.1
ruamel.yaml==0.18.5 
jinja2==3.1.3
watchdog
aiohttp
