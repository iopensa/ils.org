# Sito Italian Linux Society

Questo è il repository del sito https://www.ils.org generato staticamente con [Nikola](https://www.getnikola.com/).

## Sviluppare in locale con Docker

Scarica il repository. In seguito, da dentro la cartella principale:

```
# Aggiorna il tema (solo la prima volta)
git submodule update --init --recursive --force

# Avvio
docker-compose up --build

# Avvio (se non funziona sopra)
docker compose up --build
```

A questo punto visita questo indirizzo:

http://127.0.0.1:28000

Puoi cambiare la porta modificando il tuo file `.env`. Questa porta di default è stata scelta per evitarti collisioni se utilizzi altri repository Italian Linux Society. [Info](https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/DOCKER_GUIDELINES.md).

----

Se invece desideri generare solo la cartella `output` con Docker:

```
docker-compose run nikola build
```

Se invece NON desideri utilizzare Docker, vedi la prossima sezione.

## Avvio nativo

Requisiti di sviluppo per l'avvio nativo:

- avere un sistema operativo Unix-like qualsiasi
- python installato

Dipendenze per l'avvio nativo:

```
pip install Nikola[extras] pyyaml

git submodule update --init --recursive --force # scarica il tema di ILS

nikola build
nikola auto --browser
```

Per creare una singola build statica, eseguire questo comandi dalla cartella principale:

```
nikola build
```

## Proporre nuovi articoli

Passi consigliati per proporre velocemente nuovi articoli nel sito:

1. pubblica la bozza grezza in un Etherpad o simili per facilitare la revisione (esempio: https://etherpad.wikimedia.org/)
    - titolo
    - permalink
    - prima frase del corpo (mostrata in anteprima)
    - corpo
    - eventuale immagine di anteprima libera
2. condividila nel gruppo Telegram `@ItalianLinuxSociety` (/Comunicazione) per attrarre una prima revisione

Dopo la prima revisione:

3. pubblicate la modifica sul repository. Nella Merge Request eventualmente mettete un link al consenso già ricevuto (e.g. link al messaggio Telegram)
4. condividete l'articolo "in anteprima" a tutta la mailing list `soci` per la revisione finale - https://lists.linux.it/listinfo/soci

Dopo un giorno o due:

5. proponi dei post sui social più appropriati - https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/INVENTORY.md#social-networks-external-websites

## Segnalazioni

Per segnalazioni e richieste, apri una segnalazione:

https://gitlab.com/ItalianLinuxSociety/ils.org/-/issues/new

Oppure scrivi a:

webmaster@linux.it

Grazie!

## Licenza

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/

Eccezioni: loghi di associazioni, di partner e sponsor. Contattarli per conoscere le relative licenze.
