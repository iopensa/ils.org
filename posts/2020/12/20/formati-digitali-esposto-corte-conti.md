<!--
.. title: Formati Digitale: Esposto alla Corte dei Conti
.. slug: formati-digitali-esposto-corte-conti
.. date: 2020-12-20 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:Corte_dei_Conti_-_Sede.jpg">Carlo Dani</a>, <a rel="nofollow" href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons
.. previewimage: /images/posts/corte_dei_conti.jpg
-->


Una indagine condotta negli scorsi mesi ha fatto emergere una situazione piuttosto grave nei confronti dell'adozione del software libero e opensource da parte di centinaia di piccole amministrazioni pubbliche: uno dei maggiori fornitori di software gestionale documentale, adottato da circa 1700 comuni in tutta Italia, supporta esclusivamente documenti prodotti nei formati proprietari gestiti da Microsoft Office, di cui raccomanda caldamente l'utilizzo (alimentando inoltre la sua propria attività di rivendita di licenze).

<!-- TEASER_END -->

È stato pertanto presentato un esposto alla Corte dei Conti (precisamente alla <a rel="nofollow" href="https://www.corteconti.it/Home/Organizzazione/UfficiCentraliRegionali/UffSezContrEnti">Sezione di Controllo sugli Enti</a> e alla <a rel="nofollow" href="https://www.corteconti.it/Home/Organizzazione/UfficiCentraliRegionali/UffSezCentrContrAmm">Sezione Centrale di Controllo sulla Gestione delle Amministrazioni dello Stato</a>) in cui si invita l'Autorità ad intervenire nei confronti del suddetto fornitore per ottenere una maggiore integrazione con i formati liberi e aperti <a rel="nofollow" href="https://trasparenza.agid.gov.it/archivio19_regolamenti_0_5385.html">raccomandati dall'Agenzia per l'Italia Digitale</a>, al fine di raggiungere - con una singola azione - una più alta adesione agli standard tecnici e operativi definiti dalle norme.

Di seguito il testo inviato direttamente da Roberto Guido, presidente di Italian Linux Society:

<blockquote class="blockquote">

<p>
Io sottoscritto Roberto Guido, [omissis: dati anagrafici], segnalo una anomalia molto diffusa tra i piccoli comuni distribuiti su tutto il territorio nazionale.
</p>

<p>
Con una minima attività di ricerca ho avuto modo di individuare alcuni bandi e assegnazioni, tutti pubblicati nel corso degli ultimi 12 mesi, con i quali diversi piccoli comuni hanno richiesto e acquisito licenze dell'applicativo software Microsoft Office, in diverse sue versioni e varianti ed in diversa quantità a seconda del caso.<br>
Mi sono pertanto premurato di contattare individualmente i referenti di ciascuna gara per chiedere delucidazioni sulla mancata osservanza delle linee guida, malgrado la ben nota disponibilità di soluzioni alternative e maggiormente conformi, e le risposte ricevute (che allego a questo documento; purtroppo altri comuni contattati hanno ignorato del tutto la richiesta di accesso civico generalizzato eseguita ai sensi dell'articolo 5 del Decreto Legislativo 33/2013 e s.m.i.) sono state tra loro sorprendentemente simili: pur esistendo una varietà di fattori più o meno specifici quello espresso da tutti è l'incompatibilità del sistema gestionale informatico attualmente in uso da ciascuno con documenti generati in formati diversi da quelli propri ed esclusivi di Microsoft Office: taluni citano proprie esperienze negative avute usando altri formati digitali (che hanno comunque, di propria lodevole iniziativa, tentato di adottare), altri menzionano esplicite raccomandazioni da parte del fornitore del sistema gestionale.<br>
Indagando ulteriormente ho dovuto prendere atto che tutti i sopra citati comuni coinvolti, pur essendo distribuiti in diverse zone del nostro Paese, condividono il medesimo fornitore di servizi gestionali informatici, ovvero la società Halley Informatica s.r.l., la quale risulta (a seguito di una ricerca forse superficiale e non esaustiva, ma comunque indicativa) ricoprire lo stesso ruolo nei confronti di circa 1700 comuni italiani. Date le unanimi testimonianze allegate si può assumere che tutti questi 1700 soggetti siano altrettanto vincolati all'adozione dell'unica soluzione software supportata e pertanto alla massiccia e reiterata inosservanza di direttive governative che avrebbero altresì lo scopo di contenere e razionalizzare la spesa pubblica, armonizzare i processi ed i flussi informativi digitali e favorire la libera competizione sul mercato delle soluzioni IT.<br>
La società Halley Informatica s.r.l., contattata sia tramite e-mail che tramite PEC per avere spiegazioni sulle limitazioni del servizio da essi erogato e per ricevere una eventuale indicazione sulla futura disponibilità del supporto ai formati digitali raccomandati dall'Agenzia per l’Italia Digitale (AgID), non ha mai dato cenno di risposta.
</p>

<p>Pertanto</p>

<p>Visto</p>

<ul>
    <li>l’articolo 68 del Decreto Legislativo 82/2005 e s.m.i. (anche noto come “Codice dell’Amministrazione Digitale”), che chiede alle pubbliche amministrazioni di acquisire “programmi informatici o parti di essi nel rispetto dei principi di economicità e di efficienza, tutela degli investimenti, riuso e neutralità tecnologica, a seguito di una valutazione comparativa di tipo tecnico ed economico tra le seguenti soluzioni disponibili sul mercato”, nonché a prediligere software cosiddetto “libero” o a codice sorgente aperto;</li>
    <li>le “Linee Guida su Acquisizione e Riuso di Software per le Pubbliche Amministrazioni”, pubblicate da AgID ed in vigore dal 09/05/2019 come riportato in Gazzetta Ufficiale Serie Generale n. 119 del 23/05/2019</li>
    <li>le "Linee Guida sulla Formazione, Gestione e Conservazione dei Documenti Informatici", pubblicate da AgID ed in vigore dal 10/09/2020 come riportato in Gazzetta Ufficiale Serie Generale n. 259 del 19/10/2020</li>
    <li>il Protocollo d’Intesa “Per la promozione e il monitoraggio della trasformazione digitale della Pubblica amministrazione”, siglato il 26/11/2019 tra il Ministro per l’Innovazione Tecnologica e la Digitalizzazione Paola Pisano ed il Presidente della Magistratura Contabile Angelo Buscema finalizzato, tra le altre, a “favorire la diffusione di pratiche gestionali pubbliche che comportino risparmi di spesa e migliori performance dal punto di vista tecnologico”</li>
</ul>

<p>
Chiedo
<br>
All’Autorità in indirizzo di sollecitare, con i mezzi ed i canali che ritiene più idonei, la sopra citata società Halley Informatica s.r.l. affinché adegui il prima possibile i propri sistemi informatici ed i propri servizi digitali alle direttive in essere, garantendo pieno supporto ai formati documentali digitali espressamente raccomandati, oppure di valutare – insieme ad AgID – la revoca della qualifica di fornitore di servizi SaaS ("Software as a Service") alla stessa società Halley Informatica s.r.l., come previsto dall'articolo 6 della "Qualificazione Servizi SaaS per il Cloud della PA".
</p>

</blockquote>