<!--
.. title: LUGConf 2018
.. slug: lugconf-2018
.. date: 2018-01-30 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Sabato 24 marzo 2018 si terrà a Torino <a rel="nofollow" href="https://merge-it.net/">MERGE-it</a>, raduno nazionale delle community attive nel campo delle libertà digitali.

Per tale occasionale, Italian Linux Society invita tutti i Linux Users Groups alla LUGConf prevista per il pomeriggio: un incontro per vedersi, conoscersi, e discutere faccia a faccia alcuni dei temi più strettamente operativi di interesse generale per tutti coloro che promuovono e sostengono Linux ed il software libero nel nostro Paese.

All'ordine del giorno:
<ul>
   <li>il futuro del <a href="https://www.linuxday.it/">Linux Day</a></li>
   <li>attività locali svolte o da svolgere: esperienze, spunti e consigli</li>
   <li>assetti associativi alla luce della Riforma del Terzo Settore</li>
</ul>

Per facilitare la partecipazione dei gruppi più distanti o più piccoli, Italian Linux Society mette a disposizione 10 grants da 50 euro per sostenere almeno in parte le spese di viaggio e di alloggio; negli scorsi giorni ai LUG indicizzati sulla <a href="https://lugmap.linux.it/">LugMap</a> è stata inviata una mail di invito, con le modalità per chiedere tale contributo e per eventualmente proporre altri argomenti di discussione collettiva. Chi non avesse ricevuto suddetta mail può contattare l'indirizzo presidente@linux.it per ulteriori dettagli.