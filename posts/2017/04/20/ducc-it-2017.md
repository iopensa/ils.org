<!--
.. title: DUCC-IT 2017
.. slug: ducc-it-2017
.. date: 2017-04-20 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

<p style="text-align: center">
<img src="/images/posts/duccit.png" alt="DUCC-IT">
</p>

<p>
Sabato 6 e domenica 7 maggio si svolgerà a Vicenza l'edizione 2017 della "Debian/Ubuntu Community Conference", più nota come <a rel="nofollow" href="https://www.ducc.it/">DUCC-IT</a>, (quasi) annuale incontro tra attivisti freesoftware che negli anni si è allargata anche ad altri temi oltre le due ben note distribuzioni Linux che ne compongono il nome.
</p>

<p>
Il <a rel="nofollow" href="https://www.ducc.it/2017/programma/">programma pubblicato</a> vede, tra gli altri, <a rel="nofollow" href="https://www.ducc.it/2017/programma/dobbiamo-parlare/">un intervento</a> del Presidente di Italian Linux Society, dedicato non solo a presentare ma anche e soprattuto a discutere insieme ai partecipanti spunti e considerazioni sulla community di promozione, divulgazione e sostegno al software libero attiva nel nostro Paese.
</p>